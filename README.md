# Octree Header Library
This is a C++ octree implementation.  
Main purpose of this is to speed up search through a 3D space.  

- This implementation doesn't merge same nodes, only does *fast* search.

Probably pointless to use this for small arrays that fit into cache due to pointer traversal causing potentially lots of memory requests plus potential overhead.

## How to use
Constructor: `Octree<type>(Size);` size __Must__ be a power of 2.  
Erase Element: `Erase(x, y, z);`  
Set Element: `Set(x, y, z, value);`  
Get Element: `Get(x, y, z);`

## Specifics
Each node uses 76 bytes + sizeof(T) on a 64-bit system (9 pointers and an unsigned 32-bit + user value)  
Limiting factors for octree size are stack size and uint32_t.
