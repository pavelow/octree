/*
C++ Header Library - Octree
Implements an Octree Template for use in projects.

Daniel McCarthy 10/2018

*/

#pragma once

#include <cstdint>
#include <cassert>
#include <cmath>
#include <cstdio>

//Index Type, so we can make it more memory efficient if needed, Must be an unsigned int of some kind
#define IndexType uint64_t

template<typename T>
class Octree {
public:
  Octree(IndexType Size);
  Octree(IndexType Size, Octree* p); //Octree with parent
  ~Octree();
  void Erase(IndexType x, IndexType y, IndexType z);
  void Set(IndexType x, IndexType y, IndexType z, T value);
  T Get(IndexType x, IndexType y, IndexType z);
  bool Exists(IndexType x, IndexType y, IndexType z);
  IndexType size;
  T * Value = NULL;
private:
  Octree<T> * Traverse(IndexType x, IndexType y, IndexType z); //Get pointer to node at x,y,z
  void EraseNode(Octree<T> *); //Remove Node + Parents, if needed
  Octree<T> * Parent = NULL;
  Octree<T> * Branches [8] = {NULL};
};



template <typename T>
Octree<T>::Octree(IndexType Size) {
  assert( !(Size == 0) && !(Size & (Size - 1))); //Make sure it's a power of 2 and not zero
  size = Size;
  Value = new T(); //Use Default Constructor
}

template <typename T>
Octree<T>::Octree(IndexType Size, Octree* p) {
  assert( !(Size == 0) && !(Size & (Size - 1))); //Make sure it's a power of 2 and not zero
  size = Size;
  Parent = p;
  Value = new T(); //Use Default Constructor
}

template <typename T>
Octree<T>::~Octree() {
  //Effectivelty recurses through tree, deleting all branches
  for(int i = 0; i < 8;i++) {
    if(Branches[i] != NULL){
      delete Branches[i]; //Since Delete will call this destructor in lower branches
    }
  }
  delete Value;
}

//Traverses Octree, returns requisite branch
template <typename T>
Octree<T> * Octree<T>::Traverse(IndexType x, IndexType y, IndexType z) {
  assert(x < size && y < size && z < size);

  //First, find out which branch we need
  IndexType s = size/2;
  bool X = (x > (s-1));
  bool Y = (y > (s-1));
  bool Z = (z > (s-1));
  int i = (X*4) + (Y*2) + Z;

  if(size == 1 || Branches[i] == NULL) {
    return this;
  } else {
    return Branches[i]->Traverse(x-(X*s),y-(Y*s),z-(Z*s)); //Recurse into lower branch if it is populated
  }
}

template <typename T>
void Octree<T>::Erase(IndexType x, IndexType y, IndexType z) {
  Octree<T> * Subject = Traverse(x,y,z); //Get Thing to delete

  //If the node doesn't really exist
  if(Subject->Parent == NULL) {
    return;
  }

  //Trim Branches
  int null = 0;
  for(int i=0; i < 8; i++) {
    if(Subject->Parent->Branches[i] == NULL) {
      null++;
    } else if(Subject->Parent->Branches[i] == Subject) {
      Subject->Parent->Branches[i] = NULL; //NULL out Branch
      null++;
    }
  }
  if(null == 8) { //We Can Delete Parent
    EraseNode(Subject->Parent);
  }
  delete Subject;
}

template <typename T>
void Octree<T>::EraseNode(Octree<T> * Node) {
  //Delete Pointer back to self
  //Figure out how many null branches in parent
  int null = 0;
  for(int i=0; i < 8; i++) {
    if(Node->Parent->Branches[i] == Node) {
      Node->Parent->Branches[i] = NULL;
    }
    if(Node->Parent->Branches[i] == NULL) {
      null++;
    }
  }

  //If all branches in parent are null we can delete parent if parent is not root node
  if(null == 8) {
    if(Node->Parent->Parent == NULL) { //If parent is root node (root node has no parent)
      Node->Parent = NULL;
      delete Node;
      return;
    } else {
      //If parent is not root node, we keep recursing towards root
      EraseNode(Node->Parent);
      delete Node;
    }
  } else {
    delete Node;
  }
}

template <typename T>
void Octree<T>::Set(IndexType x, IndexType y, IndexType z, T value) {
  assert(x < size && y < size && z < size);


  if(size == 1) { //If We've reached the end, set the value
    *Value = value;
  } else {
    //Otherwise, Keep Traversing

    IndexType s = size/2;
    bool X = (x > (s-1));
    bool Y = (y > (s-1));
    bool Z = (z > (s-1));
    int i = (X*4) + (Y*2) + Z;

    if(Branches[i] == NULL) { //If the Branch Doesn't exist yet, create it
      Branches[i] = new Octree<T>(s, this);
    }
    Branches[i]->Set(x-(X*s),y-(Y*s),z-(Z*s), value);
  }
}

template <typename T>
T Octree<T>::Get(IndexType x, IndexType y, IndexType z) {
  return *Traverse(x,y,z)->Value;
}

template <typename T>
bool Octree<T>::Exists(IndexType x, IndexType y, IndexType z) {
	Octree<T>* node = Traverse(x,y,z);
	if(node->size > 1 || node->Value == NULL)
		return false;
	return true;
}
