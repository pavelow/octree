#include "octree.hpp"
#include <stdio.h>
#include <ctime>
#define OctreeSize 32
#define PopulateSize 32

int main(int argc, char ** argv) {
  printf("Test\n");
  Octree<int> A = Octree<int>(OctreeSize); // 256^3 Octree

  printf("(0,0,0)\n");
  A.Set(0,0,0, 0);
  printf("(1,0,0)\n");
  A.Set(1,0,0, 1);
  printf("(2,0,0)\n");
  A.Set(2,0,0, 2);
  printf("(3,0,0)\n");
  A.Set(3,0,0, 3);

  printf("(0,0,0) = %d\n",A.Get(0,0,0));
  printf("(1,0,0) = %d\n",A.Get(1,0,0));
  printf("(2,0,0) = %d\n",A.Get(2,0,0));
  printf("(3,0,0) = %d\n",A.Get(3,0,0));

  printf("Erase (0,0,0)\n");
  A.Erase(0,0,0);
  printf("Erase (1,0,0)\n");
  A.Erase(1,0,0);
  printf("Erase (2,0,0)\n");
  A.Erase(2,0,0);
  printf("Erase (3,0,0)\n");
  A.Erase(3,0,0);

  printf("(0,0,0) = %d\n",A.Get(0,0,0));
  printf("(1,0,0) = %d\n",A.Get(1,0,0));
  printf("(2,0,0) = %d\n",A.Get(2,0,0));
  printf("(3,0,0) = %d\n",A.Get(3,0,0));



  for(int i=0; i<PopulateSize ;i++) {
    for(int j=0; j<PopulateSize ;j++) {
      for(int k=0; k<PopulateSize ;k++) {
        A.Set(i,j,k,i+j+k);
      }
    }
  }

  A.Erase(3,0,0);

  clock_t start = clock();
  for(int i=0; i<PopulateSize ;i++) {
    for(int j=0; j<PopulateSize ;j++) {
      for(int k=0; k<PopulateSize ;k++) {
        if(A.Get(i,j,k) != i+j+k) {
          printf("Fault! %d:%d:%d (There should only be one of these :) )\n", i,j,k);
        }
      }
    }
  }
  clock_t stop = clock();

  clock_t start3 = clock();
  for(int i=0; i<PopulateSize ;i++) {
    for(int j=0; j<PopulateSize ;j++) {
      for(int k=0; k<PopulateSize ;k++) {
        if(A.Get(i,j,k) != i+j+k) {

          //printf("Fault! %d:%d:%d (There should only be one of these :) )\n", i,j,k);
        }
      }
    }
  }
  clock_t stop3 = clock();


  uint64_t l = 0;
  for(int i=0; i<PopulateSize ;i++) {
    for(int j=0; j<PopulateSize ;j++) {
      for(int k=0; k<PopulateSize ;k++) {
        l += A.Get(i,j,k);
      }
    }
  }

  for(int i=0; i<PopulateSize ;i++) {
    for(int j=0; j<PopulateSize ;j++) {
      for(int k=0; k<PopulateSize ;k++) {
        A.Erase(i,j,k);
      }
    }
  }

  clock_t start2 = clock();
  for(int i=0; i<PopulateSize ;i++) {
    for(int j=0; j<PopulateSize ;j++) {
      for(int k=0; k<PopulateSize ;k++) {
        if(A.Get(i,j,k) != 0) {
          printf("Fault! %d:%d:%d\n", i,j,k);
        }
      }
    }
  }

  clock_t stop2 = clock();

  printf("\nTest Size:%d Octree Size:%d\n", PopulateSize, OctreeSize);
  printf("Populated  : %Lfms\nPopulated 2: %Lfms\nUnpopulated: %Lfms\nSum: %d\n",
      (long double)(stop-start)/CLOCKS_PER_SEC*1000,
      (long double)(stop3-start3)/CLOCKS_PER_SEC*1000,
      (long double)(stop2-start2)/CLOCKS_PER_SEC*1000, l);
}
